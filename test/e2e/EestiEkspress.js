var conf = require('../../nightwatch.conf.js');

module.exports = {
    'Ajalugu button': function(browser){
        browser
          .url('https://ekspress.delfi.ee/')
          .resizeWindow(1920, 1080)
          .waitForElementVisible('body')
          .pause(1000)
          .click('a[href="/ajalugu"]')
          .waitForElementVisible('body')
          .pause(1000)
          .end();  
    }
}

module.exports = {
    'Delfi button': function(browser){
        browser
          .url('https://ekspress.delfi.ee/')
          .resizeWindow(1920, 1080)
          .waitForElementVisible('body')
          .pause(1000)
          .click('a[href="http://www.delfi.ee/"]')
          .waitForElementVisible('body')
          .pause(1000)
          .end();  
    }
}